#SHELL := /bin/bash
CODE_DIR=build/code/imp
PWD=$(shell pwd)
VERSION_FILE_TEMPLATE=${PWD}/src/version.org.template
VERSION_FILE=${PWD}/src/version.org
EXIT_FILE=${PWD}/exit.txt
STATUS=0
tag=$(shell git tag -l | tail -1)
export tag

all:  build

# Does all the necessary scaffolding 
init: 
	./init.sh


write-version:
	rm -rf ${VERSION_FILE}
	cp ${VERSION_FILE_TEMPLATE} ${VERSION_FILE}
	echo "\nCurrent Version is ${tag}\n" >> ${VERSION_FILE}


build: init write-version
	make -f tangle-make -k readtheorg=true all

clean:	
	make -f tangle-make clean

