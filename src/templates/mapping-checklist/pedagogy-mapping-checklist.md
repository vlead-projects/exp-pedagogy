# Pedagogy Mapping Checklist 

  The following questions form the Pedagogy mapping
  checklist.  This enables you to verify the compliance of
  your proposed experiment with the prescribed pedagogy
  structure.  Example answers to each question (with
  reference to merge sort experiment ) is also included
  below each question.
  
## Questions and Answers

   **Question 1** : Does the 'Title' cover [Bloom's
                    Taxonomy](http://www.celt.iastate.edu/teaching/effective-teaching-practices/revised-blooms-taxonomy)
                    Level 2 (Understanding)? Elaborate.

   **Answer 1** : <please place your answer here>

   **Question 2** : Does the 'Objective' cover [Bloom's Taxonomy]
                    (http://www.celt.iastate.edu/teaching/effective-teaching-practices/revised-blooms-taxonomy)
                    Level 2 (Understanding), Level 3 (Applying) and
                    Level 5 (Evaluating)?  Elaborate.

   **Answer 2** : <please place your answer here>

   **Question 3** : Does the 'Pre-Test' cover [Bloom's Taxonomy]
                    (http://www.celt.iastate.edu/teaching/effective-teaching-practices/revised-blooms-taxonomy)
                    Level 1 (Remembering), Level 2 (Understanding) and
                    Level 5 (Evaluating)?  Elaborate.

   **Answer 3** : <please place your answer here>


   **Question 4** : Does the 'Theory' cover [Bloom's
                    Taxonomy](http://www.celt.iastate.edu/teaching/effective-teaching-practices/revised-blooms-taxonomy)
                    Level 1 (Remembering), Level 2 (Understanding),
                    Level 4 (Analyzing) and Level 5 (Evaluating)?
                    Elaborate.


   **Answer 4** : <please place your answer here>

   **Question 5** : Does the 'Procedure' cover [Bloom's Taxonomy]
                    (http://www.celt.iastate.edu/teaching/effective-teaching-practices/revised-blooms-taxonomy)
                    Level 3 (Applying), Level 4 (Analyzing) and Level
                    6 (Creating)?  Elaborate.

   **Answer 5** : <please place your answer here>

  
   **Question 6** : Does the 'Simulation' cover [Bloom's Taxonomy]
                    (http://www.celt.iastate.edu/teaching/effective-teaching-practices/revised-blooms-taxonomy)
                    Level 3 (Applying), Level 4 (Analyzing), Level 5
                    (Evaluating) and Level 6 (Creating)?  Elaborate.

   **Answer 6** : <please place your answer here>

   
   **Question 7** : Does the 'Quiz' cover [Bloom's
                    Taxonomy](http://www.celt.iastate.edu/teaching/effective-teaching-practices/revised-blooms-taxonomy)
                    Level 1 (Remembering), Level 2 (Understanding),
                    Level 4 (Analyzing) and Level 5 (Evaluating)?
                    Elaborate.

   **Answer 7** : <please place your answer here>


   **Question 8** : Does the 'Feedback' cover [Bloom's
                    Taxonomy](http://www.celt.iastate.edu/teaching/effective-teaching-practices/revised-blooms-taxonomy)
                    Level 2 (Understanding) and Level 5 (Evaluating)?
                    Elaborate.
		 
   **Answer 8** : <please place your answer here>


   

  In order to help you fill the check list we encourage you to read
  the Guidelines section below and subsequently answer all the
  pedagogy process steps questionnaires in your experiment repository.
