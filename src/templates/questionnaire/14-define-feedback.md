## Guidance
   This is to get feedback from students about the lab. You can use the questions
     provided here in sample or you can ignore them altogether. This is also
     useful tool to try out a new pedagogy. You can ask specific feedback to know if
     the pedagogy is working or not. 
   
For more details follow the [link](http://community.virtual-labs.ac.in/docs/ph3-new-exp-dev/).
      
## Questions and Answers
   
   **Question 32** : What are the [default
                     questions](http://feedback.vlabs.ac.in/feedback/experiment)
                     that are included in the feedback?
     
   **Answer 32** : <please place your answer here>    

   **Question 33**: What are the new questions around pedagogy, user
                    experience, other new concepts that you have
                    included in the feedback?
		       
   **Answer 33** : <please place your answer here>
