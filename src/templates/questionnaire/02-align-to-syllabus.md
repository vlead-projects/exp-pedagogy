## Guidance
   The experiment content is checked for alignment
   against these factors:

   1. Which university/college curriculum is it aligned to?

   2. How well is the subject and curriculum of the experiment
        aligned with topics in the university curriculum?

   3. How well is the objective aligned?

   4. How well is the model and concept of the experiment aligned
        with what the curriculum specifies for the experiment?

   5. How well are the evaluation and metrics aligned?
   
For more details follow the [link](http://community.virtual-labs.ac.in/docs/ph3-new-exp-dev/).

## Questions and Answers

   **Question 5** : Provide _links_ to universities' syllabus
                    (course) where your experiment is listed
                    as a topic and _aligned_ with the curriculum?

   **Answer 5** : <please place your answer here>

   **Question 6** : How do you _align_ your experiment with [Bloom's Taxonomy]
                    (http://www.celt.iastate.edu/teaching/effective-teaching-practices/revised-blooms-taxonomy)?

   **Answer 6** : <please place your answer here>         
