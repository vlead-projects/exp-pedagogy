## Guidance
   These are the questions that are asked to the students as they are performing
   the experiment. This helps in reiterating the concept and aids the understanding. 
   This is also useful in understanding how students are learning (or not learning) from
   a particular step in the experiment. 
   
For more details follow the [link](http://community.virtual-labs.ac.in/docs/ph3-new-exp-dev/).   
  
## Questions and Answers

   **Question 26** : What in-experiment questions have you defined
                     that can be asked during the experiment to
                     evaluate student's understanding of the
                     procedure?

   **Answer 26** : <please place your answer here>
