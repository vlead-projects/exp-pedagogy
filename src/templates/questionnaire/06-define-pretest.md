## Guidance
   This is a student assessment test to evaluate the understanding of
    the student prior to performing the experiment. This is also used
    while formulating the final assessment at the end of the
    experiment. Answers of these questions help in understanding
    baseline knowledge of the student and in checking learning gains
    when used along with final ssessment.

   Following types of assessments are included:

   All pretest questions are Single or Multiple Choice Questions so
    that it is easy to assess and provide immediate feedback.
    
For more details follow the [link](http://community.virtual-labs.ac.in/docs/ph3-new-exp-dev/).    

## Questions and Answers

   **Question 10** : What questions have you included to
                    _understand_ student's expectation from
                    the experiment?
		    
   **Answer 10** : <please place your answer here>
  

   **Question 11** :  What questions have you included to
                    understand student's _prior understanding_
                    of the theory? 

   **Answer 11** : <please place your answer here>
              
   **Question 12** : What questions have you included to
                    understand student's _prior exposure_ to
                    similar experiments?
		    
   **Answer 12** : <please place your answer here>
                    
   
