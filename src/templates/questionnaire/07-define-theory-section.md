## Guidance
   The theory is the knowledge base on which the other sections of
    the experiment depend on.  Hence all knowledge related to the
    context, the performance of the experiment, constraints and
    exceptions are present in this part.

   This includes the following:

   1. Theory related to the concept being taught in the experiment.
    2. Pre-requisites for understanding the experiment.
    3. Deeper theory that is known only to the above average students.
    4. Theoritical information aligned to the learning level that is
       targeted by the experiement.
    5. Other references and related reads that will help the learner
       comprehend the concept.

   Theory is also interspersed with questions that aids the
    understanding of the theory.  These are similar to the pre-test
    questions in many cases and they can be reused.
    
For more details follow the [link](http://community.virtual-labs.ac.in/docs/ph3-new-exp-dev/).    

## Questions and Answers
   **Question 13** :  What is the _content and explanation_
                     written to relate and understand the
                     principles of the experiment?
		     
   **Answer 13** : <please place your answer here>

   **Question 14** : What are the pre-requisites for doing
                    this experiment?
    
   **Answer 14** : <please place your answer here>
    
   **Question 15** : What are references and related reading
                    included in this section?
		    
   **Answer 15** : <please place your answer here>

