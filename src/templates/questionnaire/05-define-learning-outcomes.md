## Guidance
   Learning outcomes are driven by the objectives set for the
    experiment, since the objectives also includes observable
    characteristics.  However, there are a few common outcomes that
    are always checked.

   1. Were the steps correctly executed?

   2. Was the outcome correct?

   3. Were '/what if/' scenarios performed correctly?

   4. Did the student comprehend the content?

   5. Is the student's speed of performing the experiment
       acceptable?

   6. Is the student motivated to do more?

   7. Can the student apply the procedure for new set of
       data?

   8. Are the concepts of the student in order?


## Question and Answers
   **Question 9** :What does the student _accomplish_ at the
                   end of the experiment by way of
                   understanding and applying the learnings?
                   How is this learning verified?
    
   **Answer 9** : <please place your answer here>
