## Guidance
   These are quiz questions that are asked at appropriate places in
    the theory to reinforce learning and also to evaluate how well the
    student understands the theory. Asking questions at the right
    place in reading flow is important, so do take special care in
    defining the position in the theory where a particular question is
    asked.
    
For more details follow the [link](http://community.virtual-labs.ac.in/docs/ph3-new-exp-dev/).    

## Sample

   **Question 16** : What are the questions that will be interspersed
                     in the theory section in order to _evaluate_ the
                     _understanding_ of the student while going
                     through the content?

   **Answer 16** : <please place your answer here>
  
                
   **Question 17** : Have you provided the position within the theory
                     where the question is asked?
  
   **Answer 17** : <please place your answer here>
