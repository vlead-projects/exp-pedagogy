## Guidance
     
   Guidance of pre-test section applies here as well. Also, note that depending
     on the learning level your experiment is adhering to, some of the questions
     may not be applicable. For example, if the experiment is focused on understanding
     (level 2), question about 'apply' (level 3) is not applicable. 
     
For more details follow the [link](http://community.virtual-labs.ac.in/docs/ph3-new-exp-dev/).     
     
## Sample
    
   **Question 27** : What are the questions that check recall of
                     read/used content [Bloom's
                     Taxonomy](http://www.celt.iastate.edu/teaching/effective-teaching-practices/revised-blooms-taxonomy)
                     level 1?
                   
   **Answer 27** : <please place your answer here>   

   **Question 28** : What are the questions that check the
                     understanding of the concept [Bloom's
                     Taxonomy](http://www.celt.iastate.edu/teaching/effective-teaching-practices/revised-blooms-taxonomy)
                     level 2 ?

   **Answer 28** : <please place your answer here>               



   **Question 29** : What are the questions that examine the students'
                     ability to apply the learning from the experiment
                     [Bloom's Taxonomy]
                     (http://www.celt.iastate.edu/teaching/effective-teaching-practices/revised-blooms-taxonomy)
                     level 3 ?

                   
   **Answer 29** : <please place your answer here>


   **Question 30** : What scoring mechanism is used in the quiz?
     
   **Answer 30** : <please place your answer here> 

   **Question 31**  : What assessment and marking provision
                      is used in the quiz?

   **Answer 31** : <please place your answer here>
 
