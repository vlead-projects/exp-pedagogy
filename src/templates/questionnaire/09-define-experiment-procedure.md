## Guidance
   The Procedure section has a dominant role in the effectiveness of
    the experiment.  The objectives, outcomes and the value add is
    built into the procedure.  Since this also forms the basis on
    which the simulation section of the experiment is developed, the
    core value add is embedded in this section.
    
For more details follow the [link](http://community.virtual-labs.ac.in/docs/ph3-new-exp-dev/).    

## Questions and Answers

   **Question 18** : What are the steps that the student must
                    _execute_ to _perform_ and _verify_ the
                    successful completion of the experiment?
    
   **Answer 18** : <please place your answer here>
    
   **Question 19** :  What are the additional instructions given
                    to the student to have a clear
                    understanding of the experiment
                    procedure?
		    
   **Answer 19** : <please place your answer here>
  
