
## Guidance
   Create an overview of the concept and the scope of the
   experiment. This needs you to include the following:

   **Goal** : What needs to be learned?

   **Learning level** : How much needs to be learned? You
                        may use Bloom's Taxonomy.

   **Learning Objectives and Outcomes** : Specifics of what needs
                                          to be learned and observable 
                                          outcomes to evaluate whether
                                          learning has happened.

   **Instructional Design** : How is the content going to be
                              designed so that it promotes 
                              learning through efficiency,
                              effectiness and appeal to the learner?

   **Pedagogy** : What procedure of teaching is
                  applied? Also ensure that you have
                  reviewed the recommended pedagogy and
                  are adhering to it.
                  
For more details follow the [link](http://community.virtual-labs.ac.in/docs/ph3-new-exp-dev/).

## Questions and Answers
   **Question 1** : How will you _summarize_ and state the
                    objective of the experiment in a
                    _comprehensive manner_?

   **Answer 1** : <please place your answer here>
   

   **Question 2** : State how the scope defined for the
                     experiment ensures that the content
                     enhances _understanding_ of the
                     student?
		     
   **Answer 2** : <please place your answer here>
   

   **Question 3** : What are the aspects that need _application_ of
                    understanding in the experiment?
		    
   **Answer 3** : <please place your answer here>

   **Question 4** : Explain how to _evaluate_ the performance of the
                     student in the experiment.

   **Answer 4** : <please place your answer here>
