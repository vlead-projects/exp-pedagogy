#+TITLE: New Virtual Labs Experiment Development Pedagogy - Phase III 
#+AUTHOR: VLEAD
#+DATE: [2018-01-08 Mon]
#+SETUPFILE: ./org-templates/level-0.org
#+TAGS: boilerplate(b)
#+EXCLUDE_TAGS: boilerplate
#+OPTIONS: ^:nil


* align 


** Question 1
    What are the links to universities' syllabus (course)
    where your experiment is listed as a topic?

** Question 2
    How do you align your experiment with Bloom's Taxonomy?



* LO


** Question 3
    What is a comprehensive and lucid statement of what the
    student is able to learn and demonstrate at the end of
    the experiment?



Pre-test


Question 4
   What are the questions you will ask the students about his expectations, current level of relevant knowledge and his prior experience with dong similar experiments?



Theory

Questionnaire

Question 5
   What are the  principles,  specific knowledge components, pre-requisites and further references  relevant to the experiment? Elaborate  on these.


Procedure



Question 6
   What are the steps( clear instructions)  that the student must  execute to perform the experiment correctly?

simulation 

Questionnaire

Question 7
    What are the requirements for possible  scenarios that the simulated experiment is to be developed?
   
Quiz

Question 8
What are the  questions that check the students ability to execute the experiment, his understanding and recall ablity?
Question 9
 What scoring mechanism is used in the quiz?



feedback


Question 10
    What are the questions that you will ask to get students feedback on the extent of learning, motivaiton derived and ease of doing the experiment of the student?

   
