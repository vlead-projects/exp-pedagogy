
#Questionnaire 1 - Scope and concept definition 

1. What will the student learn from the experiment ? -
   Student will learn about merge sort, how it works, and
   time complexity of the algorithm. Student will be able to
   discriminate between different sorting algorithms and be
   able to choose merge sort usage when required, and apply
   it to real-world problem like sorting the PAN card
   numbers of everyone in a district.

2. How much needs to be learned? Student should be able to
   reach level 3 of bloom's taxonomy - "Apply" Student
   should be able to understand the concept (level 2) as
   well as be able to apply their knowledge to a given
   sorting problem.

3. How is the content going to be designed so that it
   promotes learning? - We will apply Merrill's first
   principles  of instruction design [First
   Principles](http://mdavidmerrill.com/Papers/firstprinciplesbymerrill.pdf)
   We will use real-world problems, will leverage existing
   knowledge of sorting to teach merge sort, we will use
   animation to demonstrate concepts, and will let the
   learner follow merge sort by experimenting with it.
   
4. What procedure of teaching will be applied? We will apply
   the pedagogy as described in this document.

#Questionnaire 2- Align to Syllabus

1. Which Universities' syllabii's list of experiments does
   this form part of, as a topic? IIIT Hyderabad, [Data
   Structures](https://www.iiit.ac.in/academics/curriculum/undergraduate/cse/)

2. If your experiment is not aligned with the university
   syllabus, why should your experiment be included in
   Virtual lab?

3. Which pedagogical model does your proposal follow? As
   described in this document.

4. Show how you will align your experiment with Blooms
   Taxonomy? - For recall (level 1), we will help break the
   theory into easy-to-remember points and experiments will
   always end with key takeaways, for understanding (level
   2), we will use animation to show steps followed in the
   sorting process and let them run it as many times as they
   take, we will also introduce questions in the experiment
   flow to reiterate learning and point to key aspects, for
   apply (level 3), we will let the students practice with
   different data sets and also let them run the experiment
   in the way they want it to.


#Questionnaire 3 -  Experiment Title

What is the title of the experiment? - Merge Sort

#Questionnaire 4 - Learning Objectives
What will be a comprehensive and lucid  statement of what the student will be able to learn and demonstrate at the end of the experiment? - At the end of the experiment, student will be able to sort any given unsorted array using merge sort, and will also be able to demonstrate the order of complexity calculation of the algorithm.  

#Questionnaire 5 - Learning Outcome

What has the student accomplished  at the end of doing the experiment? - Student is able to recall the concept of merge sort (as evidenced by their response to post-test questions), is able to sort any unsorted array using merge sort, and is able to apply merge sort to any unordered sequence of items (where an order can be defined). See learning outcomes section in template for details. 

#Questionnaire 6 - Pre-Test
*These questions will be answered by the learner, there is nothing for lab creator to do here except providing the list of questions to be used for pre-test.  - Most learners will expect to understand the theory enough to pass the exam. Some will expect to learn how to code this algorithm and use in their program.* 

What is the learners' expectation from the experiment?  
What is the extent of understanding of the theory of the experiment?
Does the learner have experience of doing similar experience?

#Questionnaire 7- Theory

Does the content you have written relate to the principles used in the experiment? Yes
Are the  per-requisites described adequately? Yes
What are the levels of Learning  addressed in  this experiments? Level 3 (Apply) of bloom's taxonomy
What references have and related reading have you used in this section? - https://www.coursera.org/learn/algorithms-part1/lecture/ARWDq/mergesort, https://www.geeksforgeeks.org/merge-sort/, [Cormen] (https://books.google.co.in/books?hl=en&lr=&id=aefUBQAAQBAJ&oi=fnd&pg=PR5&dq=cormen+algorithm&ots=dN3sTx-Kg_&sig=XzDErCgkMwLwe7dV9_WOV1aC4Dg&redir_esc=y#v=onepage&q=cormen%20algorithm&f=false) 

#Questionnaire 8- Procedure

What are  the process steps that the student must execute to do the experiment? 1) Pick an array to sort, 2) click on 'split' to split into 2 3) Follow the step for each resultant array and keep doing till you can't do it any more 3) Drag one array on the other to merge 4) Keep doing till you get a final array of same size as original 5) You should have the sorted array now. 
What are the steps that must be used to exception scenarios? - None
Is there a clear instruction for running the experiment defined? Yes

#Questionnaire 9 - Simulation Requirements

What are the requirements of the experiments that must be used to develop the experiment simulation? - There are 4 primitives that the simulation should provide - index, compare, split, merge. It should be possible to apply these primitives to a given input (array of numbers for example), There should also be a way to record the steps followed by the student and play it back. 

What is the work flow that is required for running the simulated the experiment? Linear, see steps above. 
What other scenarios other than the normal experiment process can be executed in the experiment? Student can select different types of input arrays.
What are the  data points that need to be collected from the experiment? For each experiment, student is expected to capture the result of the operation (split or merge) and be able to count the number of operations from these records. 
What is the flexibility that the  experiment allows the  learner to control? Learner can choose the input array, learner can also choose which input(s) a primitive will be applied. In case of error, the simulation provides clear message and hint to try it better next time. 

#Questionnaire 10- Simulation

What is the architecture of the virtual experiment you have used in building the experiment? There will be a lab bench which holds the primitive operations and the inputs that can used for the experiment, it also has a 'lab floor' where experiments happen when primitives and inputs are dragged on the floor in a valid way. Output of the experiment is shown on the screen and student can record them for later use. Experiments can be recorded and played back. This is built using front-end technologies and use JS and HTML5 so that it can work well on different form factors like tablet and mobile. 
Which functionality was covered in the requirement? - All of the above as described
What in-experiment questions have you defined, in between doing, within the experiment ? A few examples: a) How many inputs does a 'merge' operation take? b) What happens when you split an array of 1 element c) What is the middle of an array with 6 elements
What metrics did you use to understand the students extent of  learning? - a) Time spent on an activity b) Correctness of output c) correctness of steps followed d) How many times student attempted a problem before getting it right. 
What are the parameters that can be measured in the experiment? a) Time spent on an activity b) Correctness of output c) correctness of steps followed d) How many times student attempted a problem before getting it right. 
What are the outcomes of the experiment ? a) Sorted array b) Recorded steps student took.

#Questionnaire 11 - Quiz 

What  questions you mentioned  in the per-assessment questionnaire  were asked in this quiz? 1, 18, 19, 20
What questions relate to recall of read/ used content and reuse? Q1, 5, 7
What questions did you ask in the quiz that examine the students ability to analyze and apply what he learn by doing the experiment? 11, 12, 14
What assessment and marking provision is made in the quiz? These are multiple choice questions and system will provide assess and provide feedback immediately, along with explanation of why the answwer is incorrect. 

#Questionnaire 12- Feedback



