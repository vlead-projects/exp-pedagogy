*Note: Checklist to help complete each of these sections can be found [here](./exp-checklist.html).*

# Define scope and concept of the experiment

### Guidance for this section
Create an overview of the concept of the experiment and the scope of the experiment so that it can be reviewed. Include the following:*
1. Goal - What needs to be learned?*
2. Learning level - How much needs to be learned? use Bloom's taxonomy
3. Learning Objectives and Outcomes - Specifics of what needs to be learned and observable outcomes to evaluate whether learning has happened
4. Instructional Design - How is the content going to be designed so that it promotes learning through efficiency, effectiness and appeal to the learner?
5. Pedagogy - What procedure of teaching will be applied?

Also ensure that you have reviewed the recommended [pedagogy](./exp-pedagogy.html) and are adhering to it. 
### Sample content
[Experiment Concept Link](./exp-concept.md)
# Align to Syllabus
### Guidance for this section
The experiment content must be checked for alignment against these factors: 
1. Which university/college curriculum is this aligned to?
2. How well is the subject and curriculum of the experiment aligned with topics in the university curriculum?
3. How well is the objective aligned?
4. How well is the model and concept of the experiment aligned with what the curriculum specifies for the experiement?
5. How well is the evaluation and metrics aligned?

### Sample content

University: **IIIT Hyderabad**
1. Subject and curriculum - **70%**
2. Objective - **50%**
3. Simulation Model - **0%**
4. Evaluation and Metrics - **90%**

# Experiment Title
### Guidance for this section
Pick a title that explains the intent well. Also pick the name for the lab and the domain it applies to. 

### Sample Content
1. Experiment Title - **Merge Sort**
2. Lab - **Data Structures**
3. Domain - **Computer Science and Engineering**

# Learning Objectives

**Experiment Section: Learning Objectives and Outcomes**

### Guidance for this section
A Learning Objective is statement that describes the specific knowledge or skills that the student needs to **acquire** and is able to  **demonstrate**. 
Here is an example learning objective:

**At the end of this lesson, the student will be able to perform the experiment on data structures with confidence and accuracy.**

Here, skill acquired is to perform an experiment, and demonstrable skill is to do them acurately and confidently. If confidence and accuracy can't be observed, objective needs to refined further and it should use different ways of observing.   

According to [[http://psycnet.apa.org/record/1974-33131-000][Gagne]], "An objective is precisely described when it communicates to another person what would have to be done to observe that a stated lesson purpose has in fact been accomplished." It should contain five components:
1. Situation
2. Learned Capability Verb (LCV)
3. Object
4. Action Verb
5. Tools, Constraints or special conditions

Gagne provides following list of learned capability verb: Discriminates, identifies, classifies, demonstrates, generates, adopts, states, executes, chooses. Action verbs can be any verb that can be observed. Here are a few examples he provides (LCV in bold, action verbs in italics): 

1. **discriminates** by *matching* French sounds of u and ou
2. **identifies** by *naming* the root, leaf, and stem of representative
plants
3. **classifies** by *using* a definition, the concept family
4. **demonstrates** by *solving* verbally stated examples, the addition
of positive and negative numbers
5. **generates** by *synthesizing* applicable rules, a paragraph describing a person's actions in a situation of fear
6. **adopts** a strategy of imagining a U.S. map, to recall the states,
in *writing* a list
7. **states** **orally** the major issues in the presidential campaign of
1932
8. **executes** by *backing* a car into a driveway
9. **chooses** by *playing* golf as a leisure activity

There are other ways of defining learning objectives, see [Mager's tips of instructional objectives](http://www2.gsu.edu/~mstmbs/CrsTools/Magerobj.html). You can also use the tool [objective builder](http://teachonline.asu.edu/objectives-builder/). 


### Sample Content

1. Given an unsorted array, demonstrates merge sort, by producing a sorted array using the pictorial representation of the process of split and merge
2. Using an unsorted array as an example, demonstrates order of complexity calculation of merge sort algorithm, by counting the # of calculations required for completing the sort

# Learning Outcomes
**Experiment Section: Learning Objectives and Outcomes**

### Guidance for this section
Learning outcomes are driven by the objectives set for the experiment, since the objective also includes observable characteristics. However, given this is an experiment, there are a few common outcomes that should always be checked. 
1. Were the steps correctly executed?
2. Was the outcome correct?
3. Were what if scenarios performed correctly?
4. Did the learner comprehend the content?
5. Is her speed of doing the experiment acceptable?
6. Is she motivated to do more?
7. Can she apply the procedure for new set of data?
8. Are her concepts in order?



### Sample Content
**Desired learning outcomes**
1. Merge sort steps were correctly executed
  1. 2-way splits during split phase was correctly executed
  2. compare and merge during merge phase was correctly execution
2. The array was sorted at the end of merge phase
3. What if scenarios on different types of inputs were performed correctly
  1. Array with only 1 element
  2. Array with 2 elements
  3. Array with pre-sorted elements
4. Learner demonstrated an understanding of the content
  1. Learner could count the number of indexing, comparing, splitting, and merging operations for a number of input arrays correctly
  2. Learner could generalize the count to show the order of complexity is O(nlogn)
5. Learner could execute the experiment at an acceptable speed (goal: >90% of attempts should be within specified time)
  1. Learner sorted an array of number within the specified time 100% of the time
  2. Learner counted the number of operations within the specified time 95% of the time
6. Learner was motivated to do more
  1. Learner tried 3-way split of arrays
  2. Learner ran the experiment with large arrays (>15 elements) multiple times
  3. Learner tried the experiement 20% more time than the average learner
7. Learner could apply the procedure for new set of data
  1. Learner could sort array of strings
  2. Learner could sort array with large arrays
8. Learner demonstrated understanding of concept
  1. Learner could compare different sorting algorithms and show what to pick when
  2. Learner could answer questions about sorting real-life data (like Aadhaar card) correctly

Specific to the objectives: 
1. Learner could easily demonstrate the working of merge sort for a given unsorted array of numbers
2. Learner could easily demonstrate how to calculate order of complexity, and calculated it correctly

# Pre-test
**Experiment Section: Pre-test**

### Guidance for this section
This is a student assessment test to estimate the extent to which the student has benefited from doing the experiment. This will also be used when formulating the final assessment at the end of the experiment. Following types of assessments should be included: 
1. What is the learners' expectation from the experiment?
2. What is the extent of understanding of the theory of the experiment?
3. Does the learner have experience of doing similar experience?

Most pre-tests will be of Multiple-Choice Questions types. 

### Sample Content
[Pre-test assessment Link]() **TBD**

# Theory
**Experiment Section: Theory**

### Guidance for this section
The theory is the knowledge base on which the rest of the experimnet sections stand. Hence all knowledge related to the context, the performance of the experiment, constraints and exceptions must be present in this part. This includes the following: 
1. Theory related to the concept being taught in the experiment
2. Pre-requisites for understanding the experiment
3. Deeper theory that will be known only to the above average learner
4. Provide theoritical information aligned to learning level that is targeted by this experiement
5. Provide other references and related reads that will help the learner comprehend the concept. 

Theory should also be interspersed with questions that aid the understanding of the theory. This will be similar to the pre-test questions in many cases, and they can be reused. 

### Sample Content
[Theory Link]() **TBD**
[Theory questions Link]() **TBD**

# Procedure
**Experiment Section: Procedure**

### Guidance for this section
The Procedure section  has a dominant role in the effectiveness of the experiment. The objectives, outcomes and the value add is to be built into the procedure. Since this also forms the basis on which the simulation section of the experiment is developed, the core value add is to be embedded in the way this section is written. The following check list covers major items to be included but innovative experiment devevlopers may have aspects added that will make a real difference to the user experience. Use the check list for guidance about writing of the procedure to execute the experiment. 

### Sample Content
[Procedure Link]() **TBD**

# Simulation 
**Experiment Section: Simulation**

### Guidance for this section
Refer to the simulation requirements checklist to get ideas on how to build the simulation. Please make sure you incorporate the recommended [pedagogy](./exp-pedagogy.html) as well. 
### Sample Content
[Simulation Design Link]() **TBD**

# Quiz
**Experiment Section: Quiz**

### Guidance for this section
Guidance of pre-test section applies here as well. 
### Sample Content
[Post-test assessment Link]() **TBD**

# Feedback
**Experiment Section: Feedback**

### Guidance for this section
There is no specific guidance, this can be same as rest of the experiments
### Sample Content
None

# Pedagogy
### Guidance for this section
When all sections have been created, it will be important to validate that the recommended [pedagogy](./exp-pedagogy.html) has been used in all parts of the experiment. Use this checklist: 
Consider these questions (also refer to the pedagogy guidelines above):
1. Is there a pre-test defined?
2. Is there a reference resource assigned for each test question (which can be offered if the answer is wrong)?
3. Are in-experiment questions defined, and location within the experiment identified?
4. Is predictive question (to be used at the beginning of the experiment) defined?
5. Are discussion points/questions defined, in case answers by students for predictive question diverge and a discussion is to be triggered)
6. Is the hypothesis clearly defined? Are there multiple hypotheses that the learner can work with? What are the assumptions being made?
7. What is the desired outcome of the experiment? Are there multiple such outcomes that are possible?
8. Is there a list of data points that need to be collected by the learner from the experiment?
9. Does the experiment allow learner to control one or more variables?
10. Is there a list of discussion points/questions defined for discussions when prediction is compared with actual output, by the students?
11. Is there a post-test defined?
12. Is there a clear instruction for running the experiment defined?
13. Can students run the experiement to discover limitations/extensions of the model?
### Sample Content
Completed checklist:
1. Is there a pre-test defined? **Yes**
2. Is there a reference resource assigned for each test question (which can be offered if the answer is wrong)?**Yes**
3. Are in-experiment questions defined, and location within the experiment identified?**Yes**
4. Is predictive question (to be used at the beginning of the experiment) defined?**Not Applicable**
5. Are discussion points/questions defined, in case answers by students for predictive question diverge and a discussion is to be triggered)**Yes**
6. Is the hypothesis clearly defined? Are there multiple hypotheses that the learner can work with? What are the assumptions being made?**Yes**
7. What is the desired outcome of the experiment? Are there multiple such outcomes that are possible?**Yes**
8. Is there a list of data points that need to be collected by the learner from the experiment?**Yes**
9. Does the experiment allow learner to control one or more variables?**No**
10. Is there a list of discussion points/questions defined for discussions when prediction is compared with actual output, by the students?**No**
11. Is there a post-test defined?**Yes**
12. Is there a clear instruction for running the experiment defined?**Yes**
13. Can students run the experiement to discover limitations/extensions of the model?**Not Applicable**
