#+title:  New Virtual Labs Experiment Development Process - Phase III
#+author: VLEAD
#+date: [2018-01-08 Mon]

#+SETUPFILE: ./org-templates/level-0.org
#+TAGS: boilerplate(b)
#+EXCLUDE_TAGS: boilerplate
#+OPTIONS: ^:nil

#+OPTIONS: ':nil *:t -:t ::t <:t H:3 \n:nil ^:t
#+OPTIONS: arch:headline author:t c:nil creator:nil
#+OPTIONS: d:(not "LOGBOOK") date:t e:t email:nil f:t
#+OPTIONS: inline:t num:t p:nil pri:nil prop:nil stat:t
#+OPTIONS: tags:t tasks:t tex:t timestamp:t title:t toc:nil
#+OPTIONS: todo:t |:t
#+LANGUAGE: en
#+SELECT_TAGS: export
#+EXCLUDE_TAGS: noexport
#+CREATOR: Emacs 24.3.1 (Org mode 8.3.4)


* Context and Preamble

Virtual Labs Project Phase II was implemented with the broad
scope of putting the labs developed by 11 participating
institutes in Phase I on to a common platform and make them
available to a larger student and teacher community and
other interested stakeholders. The experiments hosted on
cloud using Open edX as the LMS.  

During the implementation of Phase II, it was observed that
it was highly imperative to build experiments that are
aligned to the institute syllabii for the institutes to
include Virtual Labs as part of their course curriculum.
Consequently, this would assure larger usage of Virtual
Labs.  This has been the motivation for defining a process for
development of Virtual Labs experiments.  

This document aims to provide the structure of the
experiments to be developed in Phase III of the Virtual Labs
Project, as well as define a methodology for the development
of such an experiment.  Subsequent sections detail the
experiment structure and development methodology, This is
meant to serve as a guideline for the experiment developers
and associated people.

Experiments in this document refer to virtual
experiments. If you are an experiment developer and are
looking to quickly start developing one, you can skip to the
key section: [[Experiment development]]

* Organization of the document

1. Introduction
2. Objectives of Experiment development methodology
3. Frameworks and Principles used 
4. Approach and Methodology
5. Check Lists and templates 
6. References
7. Appendices

* Introduction

The Virtual Labs is a repository of about 1800 experiments
covering 11 domains, and has been used by over 80000 users.
Most of these experiments are simulated versions of what
engineering and other college students perform in physical
labs.  The virtual experiments provide the students
opportunity to repeat, reuse and study related lessons.  In
Phase II most of the 20 lakh usages recorded have been from
workshops conducted by the Virtual Lab Outreach and have
been a result of a push mode.  Pull mode usage has been low.

With skill needs and syllabii of colleges rapidly changing
across universities and autonomous colleges, it is necessary
that the experiments are also aligned with these changes to
make them useful to the student.

Alignment of experiments to the syllabii is hence a key
driver for the new lab development process.  Good user
experience, facility for the student to play around with the
simulated content are important requirements that make them
relevant and useful.  Good pedagogy to ensure learning
effectiveness is also an important requirement.
Compatibility with the tools used and Virtual labs
portability are also important considerations while
developing new experiments.

These and related reasons drive the need to develop new
experiments.  This draft document has been written with this
background. If you are looking to for guidelines and
checklists to create an experiment, you can skip to section
on "Experiment Development".

* Objectives 

The objective of this document is to describe the
methodology for development of experiments by Experiment
developers( ED) and subject matter experts(SME).

Specifically the objectives are:
1. Describe a process for developing experiments for the
   Virtual labs, that is derived using well-known
   principles.
2. Detail the process steps with necessary Check Lists,
   Templates and other required assets.
3. Provide references and reading materials around the
   principles and taxonomies used to derive the process.
 
* Frameworks and Principles used

The experiment development methodology is based on the
following models and methods:

1. Revised Blooms Taxonomy[1].
2. Kirkpatrick's model from Training Effectiveness[2].
3. Blooms Mastery model[3]. 
4. Other references specifically mentioned in the document.

* Approach and Methodology
** Structure and business process of experiment development

The existing experiments hosted in the Virtual Labs [[http://vlabs.ac.in/][site]] are
broadly divided into 6 sections- Objectives, Theory,
Procedure, Simulation, Quiz and Feedback sections. This
division of sections has been kept same in this methodolgy
document for new experiment development, for the purpose of
continuity.  Only one new section has been added namely
Pre-Test.  Accordingly the approach is described in the rest
of the document.

The business process for development and validation of the
content as defined in the pg 16 of the [[https://drive.google.com/open?id=0B1SJVUBLNVcZUS12ZzRGOVpTNmcwUHNlSm5WMGliRDkya0ZB][DPR]] of Phase 3
has also been retained as it is. 

#+caption: Experiment Development - Business Process
#+name: tbl-bus-process
#+attr_latex: :align lSSSSS

***  Experiment Development - Business Process 
|------+-------------------------+----------+------+--------------+-----------+-------------|
| Step | Process Step            | Previous | Next | Related      | Templates | Remarks     |
|      |                         |     Step | Step | Check        | (T)       |             |
|      |                         |          |      | Lists (CL)   |           |             |
|------+-------------------------+----------+------+--------------+-----------+-------------|
|      | Proposal to Lab         |    Start |    2 | ALignment CL | Alignment |             |
|    1 | Development Coordinator |          |      | Proposal CL  | Proposal  |             |
|      | (LDC) by Potential      |          |      | Experiment   |           |             |
|      | Developers              |          |      | dev.CL       |           |             |
|------+-------------------------+----------+------+--------------+-----------+-------------|
|    2 | Analysis  and Review    |        2 |    3 | Proposal     | TBD       |             |
|      | by LDC                  |          |      | Review CL    |           |             |
|------+-------------------------+----------+------+--------------+-----------+-------------|
|    3 | Modification of         |        3 | 1 or |              | TBD       |             |
|      | Proposal per review     |          |    4 |              |           |             |
|      | comments                |          |      |              |           |             |
|------+-------------------------+----------+------+--------------+-----------+-------------|
|    4 | Demo of initial set     |        3 |    5 | Demo         | TBD       |             |
|      | of experiment(s)        |          |      | review CL    |           |             |
|------+-------------------------+----------+------+--------------+-----------+-------------|
|    5 | Suggestions and         |          |      | Demo         | TBD       |             |
|      | feedback by LDC         |          |      | review CL    |           |             |
|------+-------------------------+----------+------+--------------+-----------+-------------|
|    6 | Review comments         |          |      | Demo         | TBD       |             |
|      | incorporated            |          |      | review CL    |           |             |
|------+-------------------------+----------+------+--------------+-----------+-------------|
|    7 | Review by Sudents       |          |      | Demo         | TBD       |             |
|      | and User community      |          |      | review CL    |           |             |
|------+-------------------------+----------+------+--------------+-----------+-------------|
|    8 | Goahead for full        |          |      | Payment      | Payment   |             |
|      | scale development       |          |      | releases CL  | release   | 30% Payment |
|      | of all experiments      |          |      |              | Template  |             |
|------+-------------------------+----------+------+--------------+-----------+-------------|
|    9 | Full scale development  |        8 |   10 | Exp.dev.CLs  | TBD       |             |
|      | of all experiments      |          |      |              |           |             |
|------+-------------------------+----------+------+--------------+-----------+-------------|
|   10 | Review of each by       |          |      | Review CL    | Peer      |             |
|      | peers students,         |          |      |              | review T  |             |
|      | 1-2 Domain experts.     |          |      |              |           |             |
|------+-------------------------+----------+------+--------------+-----------+-------------|
|   11 | Online feedbacks on     |          |      | TBD          | TBD       |             |
|      | quality, integration    |          |      |              |           |             |
|      | compatibility checks    |          |      |              |           |             |
|------+-------------------------+----------+------+--------------+-----------+-------------|
|   12 | Subsequent              |          |      | TBD          | TBD       |             |
|      | modifications field     |          |      |              |           |             |
|      | trials and reports      |          |      |              |           |             |
|------+-------------------------+----------+------+--------------+-----------+-------------|
|   13 | Final trial report      |          |      | TBD          | TBD       |             |
|      | submitted to LDC        |          |      |              |           |             |
|------+-------------------------+----------+------+--------------+-----------+-------------|
|   14 | Recommendaton for       |          |      | TBD          | TBD       |             |
|      | final expert evaluation |          |      |              |           |             |
|      | and recommendation      |          |      |              |           |             |
|      | for Release             |          |      |              |           |             |
|------+-------------------------+----------+------+--------------+-----------+-------------|
|------+-------------------------+----------+------+--------------+-----------+-------------|
|      | Evaluation Process      |          |      |              |           |             |
|------+-------------------------+----------+------+--------------+-----------+-------------|
|    1 | Quality/ Field Trails/  |          |      | TBD          | TBD       |             |
|      | anline feedback         |          |      |              |           |             |
|      | analysis by expert      |          |      |              |           |             |
|      | committee               |          |      |              |           |             |
|------+-------------------------+----------+------+--------------+-----------+-------------|
|    2 | Committee report        |          |      | TBD          | TBD       |             |
|      | submitted to LDC        |          |      |              |           |             |
|------+-------------------------+----------+------+--------------+-----------+-------------|
|    3 | Any modification        |          |      | TBD          | TBD       |             |
|      | requested incorporated  |          |      |              |           |             |
|      | by Proposer             |          |      |              |           |             |
|------+-------------------------+----------+------+--------------+-----------+-------------|
|    4 | Final Evaluation        |          |      | TBD          | TBD       |             |
|      | of Expert Committee     |          |      |              |           |             |
|------+-------------------------+----------+------+--------------+-----------+-------------|
|    5 | Handover to Hosting     |          |      | TBD          | TBD       |             |
|      | Team                    |          |      |              |           |             |
|------+-------------------------+----------+------+--------------+-----------+-------------|
|    6 | FOSS and Integration    |          |      | TBD          | TBD       |             |
|      | Compatibility Checks    |          |      |              |           |             |
|------+-------------------------+----------+------+--------------+-----------+-------------|

** Selection of experiments to be developed

Main aim of the process is to develop experiments that are
aligned to the curriculum.  Selection of experiments, that
are to be newly developed or old ones that are to be
modified, would be the first step.  Experiment developer
should use the following guidelines for this. 

1. Which are required by the colleges for performing in the
   semester courses but not there in the Virtual Labs,
2. Identify which experiments from the curricula of various
   colleges and univesities are not aligned to what we have
   in the Virtual labs,
3. Which virtual lab experiments need small changes or
   modifications to become aligned to their curricula,
4. Which are good to have and will be adopted or used by the
   colleges/universitiies if available.

Based on these considerations, new experiments and
modifiable old experiments are selected. This is a task that
needs good coordination with the colleges and universities.

The project DPR has clarified the approach to find
experiments to be developed or old experiments to be
modfied, "..engage with the local nodal centers and carry
out a syllabus mapping exercise to identify the gap areas in
the respective domains. These gaps will then be filled by
developing additional experiments". The intent of the
colleges to adopt the identified experiments (to use along
with the physical experiments or in place of them) will be
an important aspect in the selection of experiments to be
developed or modified.

For rest of this document, 'selected experiment' will mean
either new experiment or old experiment to be modified, that
has been selected using the guideline above.

** Approach to experiment development

In addition to learning how to do the experiment and
experience of doing of the experiment, this methodology
seeks to enable the experiment developers to include the
cognitive, affective and psychomotor aspects of Blooms
Taxonomy[4][5] . This will go a long way in enhancing the
'competence' of the students when they perform the
experiment. Competence is 'being able to
do'. Competence [5][6] is a holistic concept and is present
in everyone who has acheived some level of experience in
applying knowledge. It is a single concept that includes all
the 3 domains of Blooms Taxonomy together.

Pedagogy is an essential element of an experiment. A
recommended pedagogy is presented for use by experiment
developers, and guidelines and checklists are provided for
support.

The objective is to develop experiments that ensure
effective student competence building and good user
experience. All the sections of the experiment connect
together to form an effective learning unit. A way to
measure the effectiveness of these experiments will be
improved uUsage in a pull mode.

* Experiment development 

The focus of the methodology is to defne a pedagogy and
consequent process to develop selected experiments. One of
the key deliverables is the pedagogy so that effective
learning outcomes can be ensured.

An experiment is conceived to have 7 sections, as follows:

1. Learning Objectives and Learning Outcomes.
2. Pre-test
3. Theory related to the experiment.
4. Procedure or process steps to execute the experiment.
5. Performing the actual experiment.
6. Post-test: Answering the quiz to assess the extent of
   learning that has occured.
7. Giving Feedback on the experience of the experiment.

  
All the above are interlinked and form a complete unit from
the perspective of the learner.


The process suggested below will guide the experiment developer to
create such an experiment. Each of these steps provide a checklist and
templates defined for the purpose. Experiment developers should read
pedagogy [[./exp-pedagogy.org][guidelines]], and use the [[./exp-checklist.org][checklists]] to validate the
learnability of the experiment. When you are ready to complete the
process, use the [[./exp-template.md][template]] to get started.
 
[[./pedagogy-exp.jpg]]

*** Flowchart of Check Lists to be used in each process step
    [[http://files.virtual-labs.ac.in/exp-pedagogy/exp-pedagogy.pdf][Flowchart]]

* Next steps 
  The development of experiments may be done following the process
  steps individually Table 1 above. Each process step (column 2) can
  be defined using the corresponding checklist or other reference documents
  indicated in column 3. The [[./exp-pedagogy.org][guidelines]] document may also be
  considered in creating the experment phases. 

* Appendices
** Check List for adherence to Blooms Taxonomy- Check List (*ExD-10*)

The experiment building should address both the Learning
objectives as well as as many of the the following aspects
as feasible:

The aspects derive from Blooms revised Taxonomy covering the
Cognitive, Locomotor and the Affective domains.
|---+----+----------------+--------------------------+--------------------------+------------------------|
|   |    | Levels in      | Aspect included in IR    |                          |                        |
|   |    | in taxonomy    | Discussed in theory      | Quiz to cover            | Measurement theme      |
|   |    |                | section                  | Intermediate questions   | ( metric )             |
|---+----+----------------+--------------------------+--------------------------+------------------------|
|   |    | Cognitive      |                          |                          |                        |
|   |    | Domain         |                          |                          |                        |
|---+----+----------------+--------------------------+--------------------------+------------------------|
|   | L1 | Remembering    | defintions, fact charts, | description of  specific |                        |
|   |    |                |                          | parts covered in theory  | Ability to recall      |
|   |    |                |                          | procedure                | (explain one aspect)   |
|---+----+----------------+--------------------------+--------------------------+------------------------|
|   | L2 | Understanding  |                          | praphrases,              | can he differentiate   |
|   |    |                |                          | distinguish two concepts | / seperate 2 ideas     |
|   |    |                |                          |                          | (Yes or No or how      |
|   |    |                |                          |                          | much he can do)        |
|---+----+----------------+--------------------------+--------------------------+------------------------|
|   | L3 | Applying       |                          | apply new data to        | solve a problem        |
|   |    |                |                          | experiment scenario,     | ( use MCQ to define    |
|   |    |                |                          | simple what if questions | the solution)          |
|---+----+----------------+--------------------------+--------------------------+------------------------|
|   | L4 | Analyzing      |                          | What if sceanrio         | Perform the experiment |
|   |    | (distinguish   |                          | opinion on the experi-   | on a differeing set of |
|   |    | different      |                          | ment outcome.            | parameters             |
|   |    | parts of a     |                          |                          | ( speed at which the   |
|   |    | whole)         |                          |                          | experiment is done     |
|   |    |                |                          |                          | w.r.t expert speed)    |
|---+----+----------------+--------------------------+--------------------------+------------------------|
|   | L5 | Evaluating     |                          | Judge set of results     |                        |
|   |    | ( defending a  |                          | as  to whcih is correct; | Evaluate multiple      |
|   |    | concept/idea)  |                          | select right answers     | responses to a         |
|   |    |                |                          | based on logic           | query (% correctness) |
|---+----+----------------+--------------------------+--------------------------+------------------------|
|   | L6 | Create         |                          | Predict Outcome  of a    |                        |
|   |    | ( Creating     |                          | set of variables input   |                        |
|   |    | something new) |                          | into the experiment.     |                        |
|---+----+----------------+--------------------------+--------------------------+------------------------|
|   |    | * Affective    |                          |                          |                        |
|   |    | Domain*        |                          |                          |                        |
|---+----+----------------+--------------------------+--------------------------+------------------------|


** Factors in alignment
** Learning Outcomes - Background 
"Learning outcomes are statements that describe significant
and essential learning that learners have achieved, and can
reliably demonstrate at the end of a course or program".[5]
The approach covers- Define Objectives and consequent
Learning Outcomes ( demonstrable skill or knowledge acquired
by the student as a result of instruction).Some of the
benefits of defining Learning Outcomes are as follows.

1. Mapping learning domain levels to the learning objectives
   to help formulate and organize the content.

2. Posting assessments at various stages of the learning, to
   establish the extent of learning that the experiment
   performance has resulted in.

3. Learning and Instruction design need not cover all
   aspects and levels - only relevant ones may be covered.

4. Incorporating questions/ guidelines derived from 1 and 2
   above to help simulation section cover all suggested
   domains and levels.

5. Include the measurement of the extent of learning. This
   will reflect in the Procedure of doing the experiment.
( Source: extracted from: https://www.olabs.edu.in )
  
Guidelines for alignment of experiment to the
college/unversity syllabii:
    1. Content aligned to University curriculum,the college
       experiments, and State Board Syllabus.
    2. Features of the experiments must have the following
       characteristcs to the maximum extent possible:
    3. Interactive simulations, animations and lab videos.
    4. The concepts and understanding of the experiment.
    5. Enable Assessment of improvement in the ability to
       perform, record and learn experiments -
    6. Must be available anywhere, anytime,
    7. enhance individualised practice in all areas of
       experimentation.
    8. use of cutting edge simulation technology to create
       real world lab environments.
    9. Capture Real lab scenarios through live demonstration
       of the experiment so as to assimilate information on
       the procedures and lab equipment.
    10. Visualisation and development of the graphical
        symbols
    11. Experiment assessment can capture speed of doing

* Footnotes

[1] 'Revised Blooms Taxonomy', Center for Excellence in
 Learrning and Teaching, Iowa State University for Science
 and Technology,Copyright © 1995-2018, Iowa State University
 of Science and Technology.
 http://www.celt.iastate.edu/teaching/effective-teaching-practices/revised-blooms-taxonomy

[2] 'Kirkpatrick's Model', on the official site of
 'Kirpatrick's Partners
 https://www.kirkpatrickpartners.com/Our-Philosophy/The-Kirkpatrick-Model

[3] Daniel U. Levine (1985). Improving student achievement through
mastery learning programs. Jossey-Bass. ISBN 9780875896458.

[4] "Blooms Taxonomy",
Wikipedia. https://en.wikipedia.org/wiki/Bloom%27s_taxonomy

[5] Ravi Shankar and MGPL Narayana ‘Developing agile teams
for Project execution A Cybernetic approach’ , presented at
the Twelfth Global Flexible Systems Management Annual
Conference GLOGIFT 2012 held at the University of Vienna,
Austria, 30th July – 1st August 2012.

[6] Mark Paulk, Bill Curtis, Mary Chrissis, Weber, ‘Capability
Maturity Model for Software, Version 1.1, Technical Report CMU/SE
-93-TR-024 ESC-TR-93- 177, February 1993, Published by the SEICMU,
Pittsburgh, 15213 CMM Base document.
