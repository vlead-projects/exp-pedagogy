# Goal of the experiment
### Guidance for this section
*"A goal is statement describing the broad or abstract intent, state or condition" [Mager](https://eric.ed.gov/?id=ED065456). For example, "Learner should understand newton's first law of motion and is able to explain the phenomenon in real-world using the law" is a goal. Start with a high level goal that describes your expectation from the learners at the end of the experiment.* 

### Sample content
Learner will understand the concept of merge sort as an efficient sorting method, and will be able to apply this method to sort any input sequence of unsorted elements. Learner will also be able to explain the concept to a peer learner and demonstrate the knowledge of time and space complexity of the algorithm. 

# Learning level
### Guidance for this section
*[Bloom's taxonomy](http://www.edpsycinteractive.org/topics/cognition/bloom.html) provides with a measure of learning level that needs to be accomplished by the learner.This can also be used to define/evaluate learning objectives.*
*When creating experiment scope, consider what learning level you want the learners to achieve through this experiment. For example, most labs in current virtual labs are at level 2 (Comprehending).* 

### Sample content
The experiment will help the learner reach learning level 3 - Apply. Given any unsorted list of numbers, learner will be able to produce a sorted list by applying merge sort. Learner will also be able to sort any list of items, as long as there is an ordering defined for these items.  


# Instructional Design
### Guidance for this section
*Merrill suggests 5 first principles of instructional design:* 
*1. Learning is promoted when learners are engaged in solving real-world problems.*
*2. Learning is promoted when existing knowledge is activated as a foundation for new knowledge.*
*3. Learning is promoted when new knowledge is demonstrated to the learner.* 
*4. Learning is promoted when new knowledge is applied by the learner.* 
*5. Learning is promoted when new knowledge is integrated into the learner’s world.*
*When creating experiment concept, demonstrate how the experiment will promote the learning, by demonstrating these first principles.*

### Sample content
The experiment will use the insight that merge sort is a simple iterative application of primitives like swap, comparision, split, and merge. It will also visually demonstrate successive steps of the algorithm execution during split phase, and during merge phase. It will use animation and graphical representation of resulting binary trees to engage the student and help them understand the concept better. 

At the beginning of the experiment, we will demonstrate, through videos, how efficient sorting like merge sort can be used to sort a large list like voter id card numbers in an area so that it is easy to find a person walking in to cast their vote. We will also connect sorting with ordering students in a class by making them stand in a line and swapping with the neighbor and show how inefficient (but correct) such a sorting can be. Within the experiment, we will let the learner apply merge sort on a number of input arrays so that they can try it out themselves. We will also give an assignment to implement merge sort for producing sorted list of all applicants of aadhaar card in the city. 
