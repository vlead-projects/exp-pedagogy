#+TITLE: New Virtual Labs Experiment Development Theory Checklist - Phase III   
#+AUTHOR: VLEAD
#+DATE: [2018-01-08 Mon]
#+SETUPFILE: ./org-templates/level-0.org
#+TAGS: boilerplate(b)
#+EXCLUDE_TAGS: boilerplate
#+OPTIONS: ^:nil

* Theory checklist *ExD-03*
:PROPERTIES:
:CUSTOM_ID: ExD-03
:END:
The theory is the knowledge base on which the rest of the
experimnet sections stand. Hence all knowledge related to
the context, the performance of the experiment, constraints
and exceptions must be present in this part. This includes
alignment with curricula, and so on. The following check
list s an attempt to cover most of the points but may fall
short in specific experiments, which the expertise of the
experiment developers may fill in.

|---+----------------------------+-----------------------------------+-----------+---|
|   | Check List Item            | CL Parameter/ Metric              | Templates |   |
|---+----------------------------+-----------------------------------+-----------+---|
| 1 | Alignment with course      | Theory related to the             |           |   |
|   | objectives                 | experiment, basic concepts,       |           |   |
|   |                            | what the experiment               |           |   |
|   |                            | brings out, how it will help      |           |   |
|   |                            | in later work.                    |           |   |
|   |                            |                                   |           |   |
|   |                            | Curriculum items nearly           |           |   |
|   |                            | related to the content/exp.       |           |   |
|   |                            | Brief touchup on the  knowledge   |           |   |
|   |                            | pre-requisites for doing the      |           |   |
|   |                            | exp.                              |           |   |
|   |                            | Some aspects that will be known . |           |   |
|   |                            | only if the student is above avg. |           |   |
|---+----------------------------+-----------------------------------+-----------+---|
| 2 | Cognitive and              | Use Check List  *ExD-10*          |           |   |
|   | Psychomotor levels         | in Appendix 1                     |           |   |
|   | of Blooms  taxonomy        |                                   |           |   |
|---+----------------------------+-----------------------------------+-----------+---|
| 3 | Benefits and applicability | Cover some aspects of             |           |   |
|   |                            | benefits and applicability to     |           |   |
|   |                            | other experiments and domains     |           |   |
|---+----------------------------+-----------------------------------+-----------+---|
| 4 | Reference                  | Mention NPTEL or other            |           |   |
|   | Related Reads              | well known course material/ talks |           |   |
|   |                            | on the topic to evoke interest    |           |   |
|   |                            |                                   |           |   |
|---+----------------------------+-----------------------------------+-----------+---|

