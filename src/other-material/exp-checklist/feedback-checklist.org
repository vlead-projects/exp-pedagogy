#+TITLE: New Virtual Labs Experiment Development Feedback Checklist - Phase III   
#+AUTHOR: VLEAD
#+DATE: [2018-01-08 Mon]
#+SETUPFILE: ./org-templates/level-0.org
#+TAGS: boilerplate(b)
#+EXCLUDE_TAGS: boilerplate
#+OPTIONS: ^:nil


* Feedback Check List *ExD-08*
:PROPERTIES:
:CUSTOM_ID: ExD-08
:END:

|---+-----------------------+---------------------+---+---|
|   | Feedback Item         | Metric              |   |   |
|---+-----------------------+---------------------+---+---|
| 1 | Gather his experience | Good/ Not good      |   |   |
|   | doing the exp.        | Descriptive         |   |   |
|   |                       | Rating              |   |   |
|---+-----------------------+---------------------+---+---|
| 2 | Verify extent of      |                     |   |   |
|   | difficulty in doing   | Rating              |   |   |
|---+-----------------------+---------------------+---+---|
| 3 | Is the exp aligned to | Y/N                 |   |   |
|   | his curricula         | Extent of alignment |   |   |
|---+-----------------------+---------------------+---+---|
| 4 | Did he gain knowledge | Y/N                 |   |   |
|   | by doing the exp?     | Specific areas      |   |   |
|---+-----------------------+---------------------+---+---|
| 5 | Did it motivate him   | Y/N                 |   |   |
|   | to do more?           | Descriptive         |   |   |
|   |                       | comments            |   |   |
|---+-----------------------+---------------------+---+---|
| 6 | What are the gaps and |                     |   |   |
|   | issues with the exp.  | Descriptive         |   |   |
|---+-----------------------+---------------------+---+---|
| 7 | Overall feel          | Rating              |   |   |
|---+-----------------------+---------------------+---+---|


